Tivoli Storage Manager agent
----------------------------

To use with default values, simply add the following in your manifest:

```
include tivoli_client
```

Default core config values are as follows:

```
tivoli_client::ensure: 'present'
tivoli_client::servername: 'bars10-data'
tivoli_client::tcpserveraddress: 'bars10-data'
tivoli_client::tcpport: '1500'
tivoli_client::domain: 'all-local'
tivoli_client::nodename: $hostname
tivoli_client::passwordaccess: 'generate'
tivoli_client::memoryef: 'no'
tivoli_client::encrypt: 'no'
```

As was the case before, there are a specific set of excludes added to inclexcl,
but the rest is up to you. Defaults for those parameters are as follows:

```
tivoli_client::xfiles: []
tivoli_client::xdirectories: []
tivoli_client::xfs: []
tivoli_client::ifiles: []
```

This revamp of the module eliminates the need for inheritance. If you don't
like the defaults values, simply override them in appropriate hiera data files.

