# Set up a Tivoli backup client.
class tivoli_client (
    # push or pull the configs and packages
    $ensure = 'present',
    # BaRS server
    $servername = 'bars10-data',
    # BaRS server (can be identical to servername, or the IP)
    $tcpserveraddress = 'bars10-data',
    # BaRS server port
    $tcpport = '1500',
    # node name, as registered with BaRS
    $nodename = $::hostname,
    # Wide inclusion net; seldom do you want to stray from all-local
    $domain = 'all-local',
    # Password caching option. generate = encrypt and stow password locally.
    # prompt = prompt for password every time you connect to the server
    $passwordaccess   = 'generate',
    # Efficiency setting. no = consume all the resources it wants.
    # yes = less memory, Diskcachememory = less memory but uses more disk space
    $memoryef = 'no',
    # whether to encrypt backups to the server
    $encrypt = 'no',
    # Note that for the below, if provided, these will prepend baseline inclexcl
    $xfiles = [],       # additional files to exclude
    $xdirectories = [], # additional directories to exclude
    $xfs = [],          # additional filesystems to exclude
    $ifiles = [])       # files to explicitly include
{

  # default path for all execs
  Exec {
    path => '/bin:/usr/bin:/sbin:/usr/sbin',
  }

  #############################################################################
  # Package installation and tweaks
  #############################################################################
  include packages::compatlibs
  include packages::libstdc

  file { '/etc/tivoli':
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  $service_name = 'dsmc'

  # usual service state wrapper
  case $ensure {
    'present': {
      $service_ensure = 'running'
      $service_enable = true
    }
    'absent': {
      $service_ensure = 'stopped'
      $service_enable = false
    }
  }

  case $::osfamily {

    # RedHat-specific
    'RedHat': {
      $tivlib='/opt/tivoli/tsm/client/api/bin64'
      $tivbin='/opt/tivoli/tsm/client/ba/bin'
      $tivba='TIVsm-BA'

      package { $tivba:
        ensure  => $ensure,
        require => [Package['libstdc++'], File['/etc/tivoli']],
      }

      # soft link binaries and libs since the pkg install is inconsistent
      file {
        # binaries
        '/usr/bin/dsmadmc':
          ensure  => link,
          target  => "${tivbin}/dsmadmc",
          require => Package[$tivba];
        '/usr/bin/dsmagent':
          ensure  => link,
          target  => "${tivbin}/dsmagent",
          require => Package[$tivba];
        '/usr/bin/dsmc':
          ensure  => link,
          target  => "${tivbin}/dsmc",
          require => Package[$tivba];
        '/usr/bin/dsmcad':
          ensure  => link,
          target  => "${tivbin}/dsmcad",
          require => Package[$tivba];
        '/usr/bin/dsmj':
          ensure  => link,
          target  => "${tivbin}/dsmj",
          require => Package[$tivba];
        '/usr/bin/dsmswitch':
          ensure  => link,
          target  => "${tivbin}/dsmswitch",
          require => Package[$tivba];
        '/usr/bin/dsmtca':
          ensure  => link,
          target  => "${tivbin}/dsmtca",
          require => Package[$tivba];
        # libs
        '/usr/lib/libApiDS.so':
          ensure  => link,
          target  => "${tivlib}/libApiDS.so",
          require => Package[$tivba];
        '/usr/lib/libct_cu.so':
          ensure  => link,
          target  => "${tivlib}/libct_cu.so",
          require => Package[$tivba];
        '/usr/lib/libdmapi.so':
          ensure  => link,
          target  => "${tivlib}/libdmapi.so",
          require => Package[$tivba];
        '/usr/lib/libgpfs.so':
          ensure  => link,
          target  => "${tivlib}/libgpfs.so",
          require => Package[$tivba];
        '/usr/lib/libha_gs_r.so':
          ensure  => link,
          target  => "${tivlib}tivlib/libha_gs_r.so",
          require => Package[$tivba];
      }
    }

    # Debian-specific
    'Debian': {
      $tivba='tivsm-ba'
      $tivlib='/opt/tivoli/tsm/client/api/bin64'

      package { $tivba:
        ensure  => $ensure,
        require => File['/etc/tivoli'],
      }
    }
    # Not EL or Debian? do nothing
    default: {
    }
  }

  # symlinks for all OS'
  file {
    '/opt/tivoli/tsm/client/ba/bin/dsm.sys':
      ensure  => link,
      target  => '/etc/tivoli/dsm.sys',
      require => [ Package[$tivba] ];
    '/opt/tivoli/tsm/client/ba/bin/dsm.opt':
      ensure  => link,
      target  => '/etc/tivoli/dsm.opt',
      require => [ Package[$tivba] ];
    '/opt/tivoli/tsm/client/ba/bin/inclexcl':
      ensure  => link,
      target  => '/etc/tivoli/inclexcl',
      require => [ Package[$tivba] ];
    # This is required for RMAN backups.  Add dsm.sys under api.
    "${tivlib}/dsm.sys":
      ensure  => link,
      target  => '/etc/tivoli/dsm.sys',
      require => [ Package[$tivba] ],
  }

  # TEMPORARY. Remove the old erroneous dsmc init script.
  file { '/etc/init.d/dsmc': ensure => 'absent', }


  if $::service_provider == 'systemd' {

    # create systemd unit for OSes running it
    systemd::service { $service_name:
      ensure            => $ensure,
      description       => 'TSM Scheduler',
      after             => 'network.target',
      execstart         => '/opt/tivoli/tsm/client/ba/bin/dsmc sched',
      working_directory => '/opt/tivoli/tsm/client/ba/bin',
      remain_after_exit => true,
      restart           => 'on-failure',
      env_vars          => [ 'LANG=en_US', 'LC_ALL=en_US' ],
      wantedby          => [ 'multi-user.target' ],
      require           => Package[$tivba],
    }

    # run the service
    service { $service_name:
      ensure  => $service_ensure,
      enable  => $service_enable,
      require => Systemd::Service[$service_name],
    }

  } else {
    # Install the init script for systemv OSes
    file { '/etc/init.d/dsmc':
      ensure => $ensure,
      source => "puppet:///modules/tivoli_client/etc/init.d/dsmc.${::osfamily}",
      owner  => 'root',
      group  => 'root',
      mode   => '0755',
    }

    service { $service_name:
      ensure    => $service_ensure,
      enable    => $service_enable,
      hasstatus => true,
      require   => File['/etc/init.d/dsmcad'],
    }

  }


  #############################################################################
  # Templated config files
  #############################################################################
  file {
    '/etc/tivoli/dsm.opt':
      ensure  => $ensure,
      content => template('tivoli_client/etc/tivoli/dsm.opt.erb'),
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      require => File['/etc/tivoli'],
      notify  => Service[$service_name];
    '/etc/tivoli/dsm.sys':
      ensure  => $ensure,
      content => template('tivoli_client/etc/tivoli/dsm.sys.erb'),
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      require => File['/etc/tivoli'],
      notify  => Service[$service_name];
    '/etc/tivoli/inclexcl':
      ensure  => $ensure,
      content => template('tivoli_client/etc/tivoli/inclexcl.erb'),
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      require => File['/etc/tivoli'],
      notify  => Service[$service_name];
  }
}
